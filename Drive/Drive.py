from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
import streamlit as st

def list_drive_files(creds):
    try:
        service = build('drive', 'v3', credentials=creds)
        results = service.files().list(pageSize=10, fields="files(id, name)").execute()
        items = results.get('files', [])

        if not items:
            st.write('No files found.')
        else:
            st.write('Files:')
            for item in items:
                st.write(f"{item['name']} ({item['id']})")
    except HttpError as error:
        st.error(f'An error occurred: {error}')
