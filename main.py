# import streamlit as st
# from Authentication.Authentication import authenticate_user, get_user_email, get_user_profile
# # from Drive.Drive import list_drive_files
# from BOBTrackerChecks.Checks import QAValidator
# # Initialize session state keys if they don't exist
# if 'creds' not in st.session_state:
#     st.session_state['creds'] = None
# if 'email_address' not in st.session_state:
#     st.session_state['email_address'] = None
# if 'profile_pic_url' not in st.session_state:
#     st.session_state['profile_pic_url'] = None
#
# # Streamlit UI
# st.title("Pinterest Case Automation")
#
# # Create tabs
# tab1, tab2 = st.tabs(["Authenticate User", "Access Drive Files"])
#
# with tab1:
#     st.header("Authenticate User")
#     if st.button("Authenticate"):
#         creds = authenticate_user()
#         st.session_state['creds'] = creds
#         email = get_user_email(creds)
#         st.session_state['email_address'] = email
#         st.success(f"User authenticated successfully! Email: {email}")
#         profile_pic_url = get_user_profile(creds)
#         st.session_state['profile_pic_url'] = profile_pic_url
#         if profile_pic_url:
#             st.image(profile_pic_url, caption='Profile Picture', width=150)
#
# with tab2:
#     if st.session_state['creds']:
#         creds = st.session_state['creds']
#         # list_drive_files(creds)
#         b3_tracker_link = st.text_input("Enter BOB Tracker Drive Link")
#         if b3_tracker_link:
#             # Perform validation and automation steps using the provided link
#             st.write("Name of the File: B3 Tracker.xlsx")
#             validator = QAValidator("Overview")
#
#     else:
#         st.warning("Please authenticate the user first.")
#
# if __name__ == "__main__":
#     st.write("Streamlit app is running...")
#     # Re-authentication button

from Authentication.Authentication import authenticate_user, get_user_email
from Drive.Drive import list_drive_files
import streamlit as st
from BOBTrackerChecks.SheetsHandlerClass import GoogleSheetsHandler
import pandas as pd
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from BOBTrackerChecks.Checks import QAValidator


# Initialize session state keys if they don't exist
if 'creds' not in st.session_state:
    st.session_state['creds'] = None
if 'email_address' not in st.session_state:
    st.session_state['email_address'] = None
if 'profile_pic_url' not in st.session_state:
    st.session_state['profile_pic_url'] = None

# Streamlit UI
st.title("Pinterest Case Automation")

# Create tabs
tab1, tab2, tab3 = st.tabs(["Authenticate User", "Access BOB tracker Link", "Access drive files"])

with tab1:
    st.header("Authenticate User")
    if st.button("Authenticate"):
        creds = authenticate_user()
        st.session_state['creds'] = creds
        email = get_user_email(creds)
        st.session_state['email_address'] = email
        st.success(f"User authenticated successfully! Email: {email}")

google_sheets_handler = GoogleSheetsHandler()

with tab2:
    if st.session_state['creds']:
        google_sheets_handler.creds = st.session_state['creds']
        b3_tracker_link = st.text_input("Enter BOB Tracker Google Sheets Link",  key="b3_tracker_link")
        if b3_tracker_link:
            sheet_id = google_sheets_handler.extract_sheet_id(b3_tracker_link)
            if sheet_id:
                sheet_tabs = google_sheets_handler.get_sheet_tabs(sheet_id)
                if sheet_tabs:
                    st.write("Available Tabs:")
                tab_selected = st.selectbox("Select a tab to view data", sheet_tabs, key="tab_selected")
                if tab_selected:
                    sheet_data = google_sheets_handler.get_sheets_data_by_tab_name(sheet_id, tab_selected)
                    if sheet_data:
                        st.write(f"Data from tab '{tab_selected}':")
                        st.write(sheet_data)

    else:
        st.warning("Please authenticate the user first.")

with tab3:
    if st.session_state['creds']:
        creds = st.session_state['creds']
        drive_files_list_data = list_drive_files(creds)
        if drive_files_list_data:
            for link in drive_files_list_data:
                st.write(link)
    else:
        st.warning("Please authenticate the user first.")

if __name__ == "__main__":
    st.write("Streamlit app is running...")
