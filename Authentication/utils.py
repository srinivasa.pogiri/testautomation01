import os
import pickle

# Paths to token file
token_pickle_file = 'src/token.pickle'

def save_credentials(credentials):
    with open(token_pickle_file, 'wb') as token_file:
        pickle.dump(credentials, token_file)

def load_credentials():
    if os.path.exists(token_pickle_file):
        with open(token_pickle_file, 'rb') as token_file:
            creds = pickle.load(token_file)
        return creds
    return None
