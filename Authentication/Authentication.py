from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from .utils import save_credentials, load_credentials
from logger import logger  # Import the logger

# Define the scopes required for Google Drive and Gmail
SCOPES = ['https://www.googleapis.com/auth/drive', 'https://www.googleapis.com/auth/gmail.readonly']
# Path to client secrets file
client_secret_file = 'config/client_secret_web.json'


def authenticate_user():
    creds = load_credentials()
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(client_secret_file, SCOPES)
            creds = flow.run_local_server(port=8080)
            save_credentials(creds)
    return creds


def get_user_email(creds):
    try:
        service = build('gmail', 'v1', credentials=creds)
        profile = service.users().getProfile(userId='me').execute()
        email_address = profile.get('emailAddress')
        return email_address
    except HttpError as error:
        return None


def get_user_profile(creds):
    try:
        # Using People API to get user profile information
        service = build('people', 'v1', credentials=creds)
        profile = service.people().get(resourceName='people/me', personFields='names,emailAddresses,photos').execute()
        # Extracting the email address and profile picture URL
        profile_pic_url = profile['photos'][0]['url'] if 'photos' in profile and profile['photos'] else None
        return profile_pic_url
    except HttpError as error:
        logger.error(f'An error occurred: {error}')
        return None

def get_sheet_tabs(creds, sheet_id):
    try:
        service = build('sheets', 'v4', credentials=creds)
        sheet_metadata = service.spreadsheets().get(spreadsheetId=sheet_id).execute()
        sheets = sheet_metadata.get('sheets', [])
        sheet_names = [sheet['properties']['title'] for sheet in sheets]
        return sheet_names
    except Exception as e:
        st.error(f"An error occurred: {e}")
        return None
