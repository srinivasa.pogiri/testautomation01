import streamlit as st
from googleapiclient.discovery import build
from Authentication.Authentication import authenticate_user


class GoogleSheetsHandler:
    def __init__(self):
        self.creds = None

    def authenticate(self):
        self.creds = authenticate_user()
        return self.creds

    @staticmethod
    def extract_sheet_id(sheet_url):
        try:
            parts = sheet_url.split('/')
            if len(parts) > 5:
                return parts[5]
            else:
                st.error("Invalid Google Sheets URL. Please make sure the URL is correct.")
                return None
        except Exception as e:
            st.error(f"An error occurred while extracting sheet ID: {e}")
            return None

    def get_sheet_tabs(self, sheet_id):
        try:
            service = build('sheets', 'v4', credentials=self.creds)
            sheet_metadata = service.spreadsheets().get(spreadsheetId=sheet_id).execute()
            sheets = sheet_metadata.get('sheets', [])
            sheet_names = [sheet['properties']['title'] for sheet in sheets]
            return sheet_names
        except Exception as e:
            st.error(f"An error occurred while fetching sheet tabs: {e}")
            return None

    def get_sheets_data_by_tab_name(self, sheet_id, tab_name):
        try:
            service = build('sheets', 'v4', credentials=self.creds)
            sheet = service.spreadsheets().values().get(spreadsheetId=sheet_id, range=tab_name).execute()
            data = sheet.get('values', [])
            return data
        except Exception as e:
            st.error(f"An error occurred while fetching data from tab '{tab_name}': {e}")
            return None
