
class QAValidator:
    ERROR_CODES = ('#REF!', '#N/A')

    def __init__(self, data):
        self.data = data
        self.qa_notes = []

    def check_for_errors(self, tab_name):
        # Find the tab index based on its name
        tab_data = None
        for tab in self.data:
            if tab['name'] == tab_name:
                tab_data = tab['data']
                break

        if tab_data is None:
            print(f"Tab '{tab_name}' not found.")
            return

        # Iterate through each cell in the tab's data
        for row_index, row in enumerate(tab_data, start=1):
            for col_index, cell in enumerate(row, start=1):
                # Check for NA or reference errors
                if cell in self.ERROR_CODES:
                    # Record the error information
                    self.qa_notes.append(f"Error '{cell}' found at row {row_index}, column {col_index}")

        # Check if any errors were found
        if self.qa_notes:
            print("Errors found in the tab:")
            for note in self.qa_notes:
                print(note)
        else:
            self.qa_notes.append("No errors Found")
            print("No errors found in the tab.")

