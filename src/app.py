import os
import re
import pickle
import streamlit as st
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
import webbrowser

# Define the scopes required for Google Drive and Gmail
SCOPES = ['https://www.googleapis.com/auth/drive.readonly', 'https://www.googleapis.com/auth/gmail.readonly']

# OAuth 2.0 parameters
client_id = "1079610897554-ae7c7m7ickjp9inl0eemg85d1hklsken.apps.googleusercontent.com"
redirect_uri = "http://localhost:8080"
scope = "https://www.googleapis.com/auth/drive.readonly https://www.googleapis.com/auth/gmail.readonly"

# OAuth 2.0 authorization URL
auth_url = f"https://accounts.google.com/o/oauth2/auth?client_id={client_id}&response_type=token&redirect_uri={redirect_uri}&scope={scope}"


def extract_access_token(fragment):
    match = re.search(r'access_token=([^&]+)', fragment)
    if match:
        return match.group(1)
    return None


def save_credentials(token):
    creds = Credentials(token=token)
    with open('token.pickle', 'wb') as token_file:
        pickle.dump(creds, token_file)
    return creds


def load_credentials():
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token_file:
            creds = pickle.load(token_file)
        return creds
    return None


def authenticate_user():
    st.write("Click the button below to authorize your app:")
    if st.button("Authorize"):
        # Open the authorization URL in a web browser
        webbrowser.open(auth_url)

    st.write("After authorizing, please paste the redirected URL here:")
    redirected_url = st.text_input("Redirected URL")

    if redirected_url:
        fragment = redirected_url.split('#')[1] if '#' in redirected_url else ''
        token = extract_access_token(fragment)
        if token:
            creds = save_credentials(token)
            st.session_state['creds'] = creds
            st.success("User authenticated successfully!")
        else:
            st.error("Failed to extract access token.")


def list_drive_files(creds):
    try:
        service = build('drive', 'v3', credentials=creds)
        results = service.files().list(pageSize=2, fields="files(id, name)").execute()
        items = results.get('files', [])

        if not items:
            st.write('No files found.')
        else:
            st.write('Files:')
            for item in items:
                st.write(f"{item['name']} ({item['id']})")
    except HttpError as error:
        st.error(f'An error occurred: {error}')


# Initialize session state keys if they don't exist
if 'creds' not in st.session_state:
    st.session_state['creds'] = None
if 'email_address' not in st.session_state:
    st.session_state['email_address'] = None

# Streamlit UI
st.title("Google Drive and Gmail Manager")

# Create tabs
tab1, tab2 = st.tabs(["Authenticate User", "Access Drive Files"])

with tab1:
    st.header("Authenticate User")
    authenticate_user()

with tab2:
    st.header("Access Drive Files")
    if st.session_state['creds']:
        creds = st.session_state['creds']
        list_drive_files(creds)
    else:
        st.warning("Please authenticate the user first.")

if __name__ == "__main__":
    st.write("Streamlit app is running...")
