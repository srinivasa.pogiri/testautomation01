import os
import re
import pickle
import streamlit as st
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
import webbrowser
import json

# Define the scopes required for Google Drive and Gmail
SCOPES = ['https://www.googleapis.com/auth/drive.readonly', 'https://www.googleapis.com/auth/gmail.readonly']

# Load client secret from JSON file
CLIENT_SECRET_FILE = os.path.join('config', 'client_secret_web.json')
with open(CLIENT_SECRET_FILE) as f:
    client_secret_data = json.load(f)
client_id = client_secret_data['web']['client_id']
client_secret = client_secret_data['web']['client_secret']

# OAuth 2.0 parameters
redirect_uri = "http://localhost:8080"
scope = " ".join(SCOPES)

# OAuth 2.0 authorization URL
auth_url = f"https://accounts.google.com/o/oauth2/auth?client_id={client_id}&response_type=token&redirect_uri={redirect_uri}&scope={scope}"


# Rest of the code remains the same...


def extract_access_token(fragment):
    match = re.search(r'access_token=([^&]+)', fragment)
    if match:
        return match.group(1)
    return None


def save_credentials(token):
    creds = Credentials(token=token)
    with open('token.pickle', 'wb') as token_file:
        pickle.dump(creds, token_file)
    return creds


def load_credentials():
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token_file:
            creds = pickle.load(token_file)
        return creds
    return None


def authenticate_user():
    st.write("Click the button below to authorize your app:")
    if st.button("Authorize"):
        # Open the authorization URL in a web browser
        webbrowser.open(auth_url)

    st.write("After authorizing, you will be redirected to a page with the access token in the URL.")


def get_user_email(creds):
    try:
        service = build('oauth2', 'v2', credentials=creds)
        user_info = service.userinfo().get().execute()
        email = user_info.get('email')
        return email
    except HttpError as error:
        st.error(f'An error occurred: {error}')


# Initialize session state keys if they don't exist
if 'creds' not in st.session_state:
    st.session_state['creds'] = None

# Streamlit UI
st.title("Google Drive and Gmail Manager")

# Create tabs
tab1, tab2 = st.tabs(["Authenticate User", "Access Drive Files"])

with tab1:
    st.header("Authenticate User")
    authenticate_user()

    redirected_url = st.text_input("Paste the redirected URL here:")
    if redirected_url:
        fragment = redirected_url.split('#')[1] if '#' in redirected_url else ''
        token = extract_access_token(fragment)
        if token:
            creds = save_credentials(token)
            st.session_state['creds'] = creds
            st.success("User authenticated successfully!")
            email = get_user_email(creds)
            if email:
                st.write(f"User email: {email}")
        else:
            st.error("Failed to extract access token.")

with tab2:
    st.header("Access Drive Files")
    if st.session_state['creds']:
        creds = st.session_state['creds']
        st.write("Access token available. You can now access Drive files.")
    else:
        st.warning("Please authenticate the user first.")

if __name__ == "__main__":
    st.write("Streamlit app is running...")
