import os
import pickle
import streamlit as st
from google.oauth2.credentials import Credentials
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# Define the scopes required for Google Drive and Gmail
SCOPES = ['https://www.googleapis.com/auth/drive', 'https://www.googleapis.com/auth/gmail.readonly']

# Paths to client secrets and token files
client_secret_file = 'config/client_secret_web.json'
token_pickle_file = 'src/token.pickle'

def save_credentials(credentials):
    with open(token_pickle_file, 'wb') as token_file:
        pickle.dump(credentials, token_file)

def load_credentials():
    if os.path.exists(token_pickle_file):
        with open(token_pickle_file, 'rb') as token_file:
            creds = pickle.load(token_file)
        return creds
    return None

def authenticate_user():
    creds = load_credentials()
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(client_secret_file, SCOPES)
            creds = flow.run_local_server(port=8080)
            save_credentials(creds)
    return creds

def list_drive_files(creds):
    try:
        service = build('drive', 'v3', credentials=creds)
        results = service.files().list(pageSize=10, fields="files(id, name)").execute()
        items = results.get('files', [])

        if not items:
            st.write('No files found.')
        else:
            st.write('Files:')
            for item in items:
                st.write(f"{item['name']} ({item['id']})")
    except HttpError as error:
        st.error(f'An error occurred: {error}')

def get_user_email(creds):
    try:
        service = build('gmail', 'v1', credentials=creds)
        profile = service.users().getProfile(userId='me').execute()
        email_address = profile.get('emailAddress')
        return email_address
    except HttpError as error:
        st.error(f'An error occurred: {error}')
        return None

# Initialize session state keys if they don't exist
if 'creds' not in st.session_state:
    st.session_state['creds'] = None
if 'email_address' not in st.session_state:
    st.session_state['email_address'] = None

# Streamlit UI
st.title("Pintrest Case Automation")

# Create tabs
tab1, tab2 = st.tabs(["Authenticate User", "Access Drive Files"])

with tab1:
    st.header("Authenticate User")
    if st.button("Authenticate"):
        creds = authenticate_user()
        st.session_state['creds'] = creds
        email = get_user_email(creds)
        st.session_state['email_address'] = email
        st.success(f"User authenticated successfully! Email: {email}")

with tab2:
    st.header("Access Drive Files")
    if st.session_state['creds']:
        creds = st.session_state['creds']
        list_drive_files(creds)
    else:
        st.warning("Please authenticate the user first.")

if __name__ == "__main__":
    st.write("Streamlit app is running...")
