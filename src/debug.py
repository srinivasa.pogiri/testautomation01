import os
import pickle
import streamlit as st
from google.oauth2.credentials import Credentials
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

class GoogleManager:
    def __init__(self, client_secret_file, token_pickle_file, scopes):
        self.client_secret_file = client_secret_file
        self.token_pickle_file = token_pickle_file
        self.scopes = scopes

    def save_credentials(self, credentials):
        with open(self.token_pickle_file, 'wb') as token_file:
            pickle.dump(credentials, token_file)
        return credentials

    def load_credentials(self):
        if os.path.exists(self.token_pickle_file):
            with open(self.token_pickle_file, 'rb') as token_file:
                creds = pickle.load(token_file)
            return creds
        return None

    def authenticate_user(self):
        creds = self.load_credentials()
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(self.client_secret_file, self.scopes)
                creds = flow.run_local_server(port=8080)
                self.save_credentials(creds)
        return creds

    def list_drive_files(self, creds):
        try:
            service = build('drive', 'v3', credentials=creds)
            results = service.files().list(pageSize=10, fields="files(id, name)").execute()
            items = results.get('files', [])

            if not items:
                st.write('No files found.')
            else:
                st.write('Files:')
                for item in items:
                    st.write(f"{item['name']} ({item['id']})")
        except HttpError as error:
            st.error(f'An error occurred: {error}')


# Initialize session state keys if they don't exist
if 'google_manager' not in st.session_state:
    st.session_state['google_manager'] = GoogleManager('config/client_secret_web.json', 'token.pickle', [
        'https://www.googleapis.com/auth/drive',
        'https://www.googleapis.com/auth/gmail.readonly'
    ])

google_manager = st.session_state['google_manager']

# Initialize 'creds' key in session state
if 'creds' not in st.session_state:
    st.session_state['creds'] = None

# Streamlit UI
st.title("Google Drive and Gmail Manager")

# Create tabs
tab1, tab2 = st.tabs(["Authenticate User", "Access Drive Files"])

with tab1:
    st.header("Authenticate User")
    if st.button("Authenticate"):
        creds = google_manager.authenticate_user()
        # display_name, profile_pic_url = google_manager.get_user_profile(creds)
        # st.session_state['display_name'] = display_name
        # st.session_state['profile_pic_url'] = profile_pic_url
        st.session_state['creds'] = creds  # Update creds in session state
        # st.success(f"User authenticated successfully! Name: {display_name}")
        # if profile_pic_url:
        #     st.image(profile_pic_url, caption='Profile Picture', width=150)

with tab2:
    st.header("Access Drive Files")
    if st.session_state['creds']:
        creds = st.session_state['creds']
        google_manager.list_drive_files(creds)
    else:
        st.warning("Please authenticate the user first.")

if __name__ == "__main__":
    st.write("Streamlit app is running...")
